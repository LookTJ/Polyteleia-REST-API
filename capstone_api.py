#!/usr/bin/env python
from flask import Flask
from flask import g
from flask import Response
from flask import request
from flask import jsonify
import json
import MySQLdb
from pprint import pprint
import serial
import time
import config

app = Flask(__name__)

@app.before_request
def db_connect():
  g.conn = MySQLdb.connect(host=config.DATABASE_CONFIG['host'],
                              user=config.DATABASE_CONFIG['user'],
                              passwd=config.DATABASE_CONFIG['password'],
                              db=config.DATABASE_CONFIG['dbname'])
  g.cursor = g.conn.cursor()

@app.after_request
def db_disconnect(response):
  g.cursor.close()
  g.conn.close()
  return response

def query_db(query, args=(), one=False):
  g.cursor.execute(query, args)
  rv = [dict((g.cursor.description[idx][0], value)
  for idx, value in enumerate(row)) for row in g.cursor.fetchall()]
  return (rv[0] if rv else None) if one else rv

@app.route("/")
def hello():
  return "Hello World!"

@app.route("/drinks", methods=['GET'])
def drinks():
  result = query_db("SELECT drink_id,drink_name,drink_path FROM drinks")
  data = json.dumps(result)
  resp = Response(data, status=200, mimetype='application/json')
  return jsonify({"success": 1, "drinks": result})

@app.route("/add", methods=['POST'])
def add():
  req_json = request.get_json()
  pprint(req_json['drink_name'])
  g.cursor.execute("INSERT INTO drinks (drink_name, drink_path) VALUES (%s, %s)", [req_json['drink_name'], req_json['drink_path']])
  g.conn.commit()
  pprint(req_json)
  resp = Response("Updated", status=201, mimetype='application/json')
  return jsonify({ "drink_name": req_json['drink_name'], "drink_path": req_json['drink_path'] })

@app.route("/drinks/<int:drink_id>", methods=['PUT'])
def update_drink(drink_id):
    req_json = request.get_json()
    pprint(req_json)
    g.cursor.execute("UPDATE drinks SET drink_name = %s, drink_path = %s WHERE drink_id = %s", [req_json['drink_name'], req_json['drink_path'], [drink_id]])
    g.conn.commit()
    return jsonify({ "drink_id": drink_id, "drink_name": req_json['drink_name'], "drink_path": req_json['drink_path'] })

@app.route("/drinks/<int:drink_id>", methods=['POST'])
def create_drink(drink_id):
    g.cursor.execute("SELECT drink_path from drinks WHERE drink_id = %s", [drink_id])
    row = g.cursor.fetchone()
    print row[0]
    ser = serial.Serial('/dev/ttyACM0', 9600)
    time.sleep(1)
    time.sleep(1)
    print(ser.name)
    ser.write(row[0])
    ser.close
    resp = Response("Tested", status=201, mimetype='application/json')
    return jsonify({"result": True})


@app.route("/drinks/<int:drink_id>", methods=['DELETE'])
def delete_drink(drink_id):
    g.cursor.execute("DELETE FROM drinks WHERE drink_id=%s", [drink_id])
    g.cursor.execute("SET @num := 0")
    g.cursor.execute("UPDATE drinks SET drink_id = @num := (@num+1)")
    g.cursor.execute("alter table drinks auto_increment = 1")
    g.conn.commit()
    return jsonify({"result": True})
    

if __name__ == "__main__":
    app.run(host='::')
# :: - supports ipv6, 0.0.0.0 - ipv4 only.
