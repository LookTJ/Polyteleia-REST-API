if not __name__.endswith("sample_config"):
    import sys
    print("The README is there to be read. Extend this sample config to a config file, don't just rename and change "
          "values here. Doing that WILL backfire on you.\nServer quitting.", file=sys.stderr)
    quit(1)

# create a new config.py and extend this data structure.
DATABASE_CONFIG = {
        'host': 'HOST', # usually mysql is localhost
        'dbname': 'DATABASE NAME',
        'user': 'USERNAME',
        'password': 'YOUR PASSWORD'
}
